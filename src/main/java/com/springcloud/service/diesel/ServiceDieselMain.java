package com.springcloud.service.diesel;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringCloudApplication
@EnableEurekaClient
@EnableFeignClients
public class ServiceDieselMain {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ServiceDieselMain.class, args);
    }
}
