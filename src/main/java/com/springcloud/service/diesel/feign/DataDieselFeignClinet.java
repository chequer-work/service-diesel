package com.springcloud.service.diesel.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @Author: diesel
 * @Description:
 * @Date: Created in 2018/1/25 ${Time}.
 * @Modified : By [author] in [date]
 */
@FeignClient(name = "data-diesel")// 服务名不能有"_"
public interface DataDieselFeignClinet {
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    Map user(@RequestParam(value = "userId") String userId);

    @RequestMapping(value = "/company", method = RequestMethod.GET)
    List company();
}
