package com.springcloud.service.diesel.controller;

import com.springcloud.service.diesel.feign.DataDieselFeignClinet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: diesel
 * @Description:
 * @Date: Created in 2018/1/25 ${Time}.
 * @Modified : By [author] in [date]。
 */

@RestController
public class UserController {
    @Autowired
    private DataDieselFeignClinet clinet;

    @Value("${server.port}")
    private String port;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Map user(String userId) {
        System.out.println(userId);
        Map map = clinet.user(userId);
        map.put("portser", port);
        return map;
    }
}
